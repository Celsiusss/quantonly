
from slack_sdk import WebClient

class BotFather():
    """
    Use this class to send messages, this is a default setup.
    """
    def __init__(self, slack_token:str, channel:str, debug:bool=False)->None:
        
        self.debug = debug
        self.slack_token = slack_token
        if(self.debug):
            self.channel = 'C05TYUVBG7M'
        else:
            self.channel = channel


    def sendMessage(self, message:str)->None:

        client = WebClient(token=self.slack_token)
        response = client.chat_postMessage(
            channel=self.channel, 
            text=message
        )







