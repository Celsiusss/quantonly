from data_pull.authenticate import DataPuller, BetAndSeatMerger
from trackUser import checkHost
import datetime
from botFather import BotFather

import numpy as np
import awswrangler as wr
import pandas as pd


class MergeBetSeatData:

    """
    Class to merge betting and seat data such that each bet is
    assigned to an account in a recorded seat. (KRB).

    Args:
        bets: Public betting data.
        seats: Public seating data.
    """

    def __init__(
        self,
        bets: pd.DataFrame,
        seats: pd.DataFrame,
    ) -> None:
        self.bets = bets
        self.seats = seats

    def parse_bets_and_seats(self) -> int:
        """Merges the bets and seats data to have a account_id tagged to each
        bet.
        """
        all_seats = self.unpack_seats(self.seats)

        self.bets["seat"] = self.bets["position"].apply(self.parse_position)
        self.bets = self.bets.drop("position", axis=1)

        all_bets = self.merge_and_fill(self.bets, all_seats)
        return all_bets

    def parse_position(self, position: int) -> int:
        """Convert string format seat_n into integer n

        Args:
            position (string): String position 'seat_n'

        Returns:
            Integer seat number
        """
        if type(position) == str:
            if position.startswith("seat"):
                return int(position.split("_")[1])
            return None
        return position

    def unpack_seats(self, seats: pd.DataFrame) -> pd.DataFrame:
        """Convert the different seat columns into a single numbered column.

        Args:
            seats: Public seat data.

        Returns:
            Dataframe of seat data with a numbered single seat column
        """
        # convert our different seat columns to a single column
        all_seats = None
        cols = [f"seats_{i}" for i in range(7)]

        for c in cols:
            seat_number = int(c.split("_")[1])
            seat = seats[["time", "table_id"]]
            seat["account_id"] = seats[c]
            seat["seat"] = seat_number
            if all_seats is None:
                all_seats = seat
            else:
                all_seats = pd.concat([all_seats, seat])

        return all_seats

    def merge_and_fill(
        self, bets: pd.DataFrame, all_seats: pd.DataFrame
    ) -> pd.DataFrame:
        """Merge bets and seats public data. We loop over table and seats, sort
        by time and fill in empty account names.

        Args:
            bets: Public betting data
            all_seats: Unpacked seat data

        Returns:
            Merged bets and seat data.
        """
        all_bets = None
        for table_id in bets["table_id"].unique():
            # print(table_id)
            for seat in bets["seat"].unique():
                bets1 = bets[(bets["table_id"] == table_id) & (bets["seat"] == seat)]
                if len(bets1) == 0:
                    continue
                seats1 = all_seats[
                    (all_seats["table_id"] == table_id) & (all_seats["seat"] == seat)
                ]
                seats1["account_id"] = seats1["account_id"].fillna("")
                bets_merged = pd.concat([bets1, seats1])
                bets_merged = bets_merged.sort_values(by="time", ascending=True)
                bets_merged["account_id"] = bets_merged["account_id"].fillna(
                    method="ffill"
                )
                bets_merged = bets_merged[~bets_merged["_gameid"].isnull()]
                if all_bets is None:
                    all_bets = bets_merged
                else:
                    all_bets = pd.concat([all_bets, bets_merged])

        return all_bets.dropna(subset=["account_id"])


def get_bet_seat_data(bf_solo: BotFather, days: int = 1) -> None:
    """Pull nessacary data from the data base and merge.

    Args:
        bf_solo (_type_): _description_
        days (int, optional): _description_. Defaults to 1.
    """
    import warnings

    warnings.filterwarnings("ignore")

    start_date = datetime.date.today() - datetime.timedelta(days=3 + days)
    end_date = datetime.date.today() - datetime.timedelta(days=3)
    date_range = pd.date_range(start=start_date, end=end_date)
    date_list = date_range.strftime("%Y-%m-%d").tolist()
    bf_solo.sendMessage(f"Running merger from{start_date} to {end_date}")

    for time_string in date_list:
        ymd = time_string.split("-")

        my_filter = (
            lambda x: x["year"] == ymd[0]
            and x["month"] == ymd[1]
            and x["day"] == ymd[2]
        )
        filter_bets = wr.s3.read_parquet(
            "s3://archivebucke7/blackjack_bets/",
            dataset=True,
            partition_filter=my_filter,
        )
        filter_seats = wr.s3.read_parquet(
            "s3://archivebucke7/blackjack_seats/",
            dataset=True,
            partition_filter=my_filter,
        )

        filter_bets[
            ["id", "author", "table_id", "_gameid", "type", "combination", "user_id"]
        ] = filter_bets[
            ["id", "author", "table_id", "_gameid", "type", "combination", "user_id"]
        ].astype(
            object
        )
        filter_bets["user_id"] = np.nan
        filter_bets["seat"] = filter_bets["position"].astype(int)
        filter_bets["position"] = filter_bets["position"].astype(int)
        # filter_bets = filter_bets.drop(columns=['year','month','day'])

        merger = MergeBetSeatData(bets=filter_bets, seats=filter_seats)
        merged_df = merger.parse_bets_and_seats()
        merged_df.to_csv(
            f"C:/Users/Mobile_Seventeen/mmartin/dailyData/merged/merged_{str(time_string)}.csv"
        )
    bf_solo.sendMessage(
        f"Hey top boys, finished running merger from {start_date} to {end_date}"
    )


def main():
    import sys

    script_name = sys.argv
    token = "xoxb-1335590872306-5950116291923-GcQ5jkfxmkjypPeArvW0GkqJ"
    channel = "C05SYG94K0A"
    bf_solo = BotFather(token, channel, debug=True)

    if len(script_name) > 3:
        raise ValueError("You have inputed to many variables")

    if script_name[1].lower() == "merger":
        try:
            lookback = int(script_name[2])
            print("ruuning bets and seat merger")
            betsAndSeats(bf_solo, days=lookback)
        except Exception as e:
            bf_solo.sendMessage(f"<!here> Hey fuckface merger failed")

    else:
        print("Running nothing could not decrypte your parameters")


if __name__ == "__main__":
    main()
