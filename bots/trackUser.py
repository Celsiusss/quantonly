import time
import sys
import logging
import psycopg2
import datetime
import pandas as pd
from botFather import BotFather
from data_pull.authenticate import DataPuller


def connect_to_main():
    conn_main = psycopg2.connect(
                user='lordkelvin',
                password='xCqabeV4aZMWL8',
                host='35.189.89.101',
                # host='35.242.137.184',
                port='5432',
                database='livecasino',
                sslmode='verify-ca',
                sslrootcert= '/home/kelvin/.keys/server-ca.pem',
                sslcert= '/home/kelvin/.keys/client-cert.pem',
                sslkey= '/home/kelvin/.keys/client-key.pem',
            )
    conn_main.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor_main = conn_main.cursor()
    return conn_main, cursor_main

def pull_data(query, cursor):
    cursor.execute(query)
    cols = cursor.description
    cols = [c.name for c in cols]
    data = cursor.fetchall()
    data = pd.DataFrame(data)
    data.columns = cols
    
    return data


query, cursor = connect_to_main()


def checkHost():
    import socket

    # Get the hostname of the local machine
    hostname = socket.gethostname()
    print(hostname)
    if(hostname == 'LAPTOP-4FE49SP5'):
        return 'max'
    elif(hostname == 'Mobile-Seventee'):
        return ' Mobile-Seventee'

def updateWhenUserPlays(user_wanted, token, channel):
    

    bf_solo = BotFather(token, channel, debug=True)
    bf_solo.sendMessage(f'<!here> Bot running...\nWill send update when user "{user_wanted}" joins a table.')

    bf = BotFather(token, channel)
    dp = DataPuller(checkHost())

    scraping_query = """SELECT * from scraping_tables"""
    table_names = dp.pull_data(scraping_query)



    while(True):

        current_time = datetime.datetime.now()
        five_minutes_ago = (current_time - datetime.timedelta(minutes=10)).strftime("%Y-%m-%d %H:%M:%S")
        
        time.sleep(60)
        pd.set_option('display.max_rows', 100)
        seats_query = f"SELECT * from blackjack_seats WHERE timestamp_written >= '{five_minutes_ago}'"
        seats = dp.pull_data(seats_query)
        seats_with_user = seats[seats.apply(lambda row: row.str.contains(user_wanted).any(), axis=1)]
        if(len(seats_with_user) > 0):
            
            user_time = seats_with_user.sort_values(by='time').iloc[[-1]]['time'].iloc[0]
            table = seats_with_user.sort_values(by='time').iloc[[-1]]['table_id'].iloc[0]
            table_name = table_names[table_names['table_id'] == table]['table_name'].iloc[0]
            mesage_to_send = f'<!here> USER: {user_wanted} has been spotted at table: {table_name} at {user_time}'
            bf.sendMessage(mesage_to_send)
            bf.sendMessage('Will now pause bot for one hour')
            time.sleep(3600)


def get_user_stats(user):
    
    import warnings
    warnings.filterwarnings('ignore')
    import datetime
    import time
    from bots.botFather import BotFather
    from data_pull.authenticate import DataPuller, BetAndSeatMerger

    # user = 'rlvxemekdnsrmdek'
    
    dp = DataPuller(checkHost())

    token = 'xoxb-1335590872306-5950116291923-GcQ5jkfxmkjypPeArvW0GkqJ'
    channel = 'C05SYG94K0A'
    bf_solo = BotFather(token, channel, debug=True)
    bf = BotFather(token, channel)

    bf_solo.sendMessage(f'<!here> {user} todays betting stats on the way...')

    merger = BetAndSeatMerger()

    scraping_query = """SELECT * from scraping_tables"""
    table_names = dp.pull_data(scraping_query)

    current_time = datetime.datetime.now()
    current_date = current_time.strftime('%Y-%m-%d')

    bets_query = f"SELECT * from blackjack_bets WHERE timestamp_written > '{current_date} 00:00:00'"
    bets = dp.pull_data(bets_query)
    bets = bets.drop_duplicates(subset=['table_id', '_gameid', 'position', 'type'])
    amount_bets = len(bets)

    if amount_bets > 0:

        seats_query = f"SELECT * from blackjack_seats WHERE timestamp_written > '{current_date} 00:00:00'"
        seats = dp.pull_data(seats_query)

        merged = merger.parse_bets_and_seats(bets, seats)

        merged_user = merged[merged['account_id'] == user]
        merged_user['PnL'] = merged_user['payout'] - merged_user['amount']
        merged_user['Number_main_bets'] = len(merged_user[merged_user['type'].str.contains('main')])
        merged_user['Number_side_bets'] = len(merged_user[~merged_user['type'].str.contains('main')])
        merged_user_grouped = merged_user.groupby('account_id').agg({'PnL': 'sum', 'Number_main_bets': 'first', 'Number_side_bets': 'first'}).reset_index()

        
        bf_solo.sendMessage(merged_user_grouped.to_string(index=False))

    else:
        bf_solo.sendMessage(f'{user} has not bet today!')





def updateWhen10kBet():
        
    import warnings
    warnings.filterwarnings('ignore')
    import datetime
    import time
    from bots.botFather import BotFather
    from data_pull.authenticate import DataPuller, BetAndSeatMerger, MergeBetSeatData

    token = 'xoxb-1335590872306-5950116291923-GcQ5jkfxmkjypPeArvW0GkqJ'
    channel = 'C05SYG94K0A'
    bf_solo = BotFather(token, channel, debug=True)
    bf = BotFather(token, channel)

    bf_solo.sendMessage(f'<!here> 10k bet tracker running...')
    stake_amount = 10000
    dp = DataPuller(checkHost())


    while(True):
        
        merger = BetAndSeatMerger()
        
        scraping_query = """SELECT * from scraping_tables"""
        table_names = dp.pull_data(scraping_query)

        current_time = datetime.datetime.now()
        five_minutes_ago = (current_time - datetime.timedelta(minutes=1)).strftime("%Y-%m-%d %H:%M:%S")

        bets_query = f"SELECT * from blackjack_bets WHERE timestamp_written >= '{five_minutes_ago}'"
        bets = dp.pull_data(bets_query)
        bets = bets.drop_duplicates(subset=['table_id', '_gameid', 'position', 'type'])
        amount_bets = len(bets[bets['amount'] >= stake_amount])
        if(amount_bets > 0):

            seats_query = f"SELECT * from blackjack_seats WHERE timestamp_written >= '{five_minutes_ago}'"
            seats = dp.pull_data(seats_query)

            merged = merger.parse_bets_and_seats(bets, seats)
            
            table_name_string = f"""WARNING we have {len(merged[merged['amount'] >= stake_amount])} bets worth more than {stake_amount}
            """

            for index, tables in merged[merged['amount'] >= stake_amount].iterrows():
                table_name_print = table_names[table_names['table_id'] == tables['table_id']]['table_name'].iloc[0]
                table_name_string = f"User:{tables['account_id']} has just bet >={stake_amount} on {table_name_print} at seat:{tables['seat']}"

            bf.sendMessage(table_name_string)

            time.sleep(51)
        time.sleep(10)


def launch10KBot():
    import time
    try:
        updateWhen10kBet()
    except Exception as e:
        bf = BotFather(token, 'C05TYUVBG7M')
        bf.sendMessage(f'<!here> Max your 10k bot has crashed\n{e}\nRestarting...')
        time.sleep(10)
        launch10KBot(user_wanted)



def launchBot(user_wanted):
    import time
    #Bot Top_user
    token = 'xoxb-1335590872306-5950116291923-GcQ5jkfxmkjypPeArvW0GkqJ'
    #quant analysis channel
    channel = 'C05SYG94K0A'

    print(f'Tracking user:{user_wanted}')
    
    try:
        updateWhenUserPlays(user_wanted, token, channel)
    except Exception as e:
        bf = BotFather(token, 'C05TYUVBG7M')
        bf.sendMessage(f'<!here> Max your user tracker bot has crashed\n{e}\nRestarting bot...')
        time.sleep(10)
        launchBot(user_wanted)


def launchUserStatsBot(user_wanted):
    import time
    #Bot Top_user
    token = 'xoxb-1335590872306-5950116291923-GcQ5jkfxmkjypPeArvW0GkqJ'
    #quant analysis channel
    channel = 'C05SYG94K0A'

    print(f'Getting {user_wanted} todays stats')
    
    try:
        get_user_stats(user_wanted)
    except Exception as e:
        bf = BotFather(token, 'C05TYUVBG7M')
        bf.sendMessage(f'<!here> Max your user tracker bot has crashed\n{e}\nRestarting bot...')
        time.sleep(5)
        launchBot(user_wanted)



def main():

    import sys
    script_name = sys.argv
    print(script_name[1])
    if(len(script_name)>3):
        raise ValueError('You have inputed to many variables')

    if(script_name[1].lower() == 'track'):
        user_wanted = script_name[2]
        
        launchBot(user_wanted)
    
    elif script_name[1] == 'get_user_stats':
        user_wanted = script_name[2]
        launchUserStatsBot(user_wanted)
        

    elif(script_name[1].lower() == '10k'):
        launch10KBot()

    else:
        print('Running nothing could not decrypte your parameters')

if __name__ == "__main__":
    main()