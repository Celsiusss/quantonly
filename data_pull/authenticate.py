import pandas as pd
import psycopg2
import pickle 

class DataPuller():
    """
    Simple Helper method used to easily retreive data.
    You need to hardcode the path in which you store your own pickle file with user/password.
    Please ask Maxime if you need a template
    """

    def __init__(self, user):
        self.user = user
        self.user_name, self.password = self.get_user_credential(self.user)
        self.cursor = self.connect_to_main()
        self.ssl_path = ''

    def get_user_credential(self, user):
        """
        This is where you hard code youre path depedning on the user
        """
        path = ''
        if(user == 'max'):
            path = 'C:/Users/Maxime Martin/Documents/Onboarding process/'
        elif(user == ' Mobile-Seventee'):
            path = 'C:/Users/Mobile_Seventeen/mmartin/login/'
            #Find the pickle file but use max's credentials
            self.user = 'max'
        elif user == 'kelvin':
            path = '/home/kelvin/whiteswan/pkl/'
        else:
            raise ValueError(f'The user {user} was not mapped!')
        
        with open(path+'user_names.pkl', 'rb') as pickle_file:
            user_name = pickle.load(pickle_file)


        with open(path+'user_password.pkl', 'rb') as pickle_file:
            user_password = pickle.load(pickle_file)

        return user_name, user_password


    def connect_to_main(self):
        if self.user == 'kelvin':
            keys_path = '/home/kelvin/.keys/'
            sslrootcert = keys_path + 'server-ca.pem'
            sslcert = keys_path + 'client-cert.pem'
            sslkey = keys_path + 'client-key.pem'
        else:
            sslrootcert='./data_pull/SSL files/server-ca.pem',
            sslcert='./data_pull/SSL files/client-cert.pem',
            sslkey='./data_pull/SSL files/client-key.pem'
    
        conn_main = psycopg2.connect(
                    #user='maxijazz',
                    #password='9[v#|4YJ,,)y@#&o',
                    user=self.user_name[self.user],
                    password=self.password[self.user],
                    host='35.189.89.101',
                    port='5432',
                    database='livecasino',
                    sslmode='verify-ca',
                    sslrootcert=sslrootcert,
                    sslcert=sslcert,
                    sslkey=sslkey
                )
        conn_main.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        cursor_main = conn_main.cursor()
        return cursor_main


    def pull_data(self, query):
        self.cursor.execute(query)
        cols = self.cursor.description
        cols = [c.name for c in cols]
        data = self.cursor.fetchall()
        data = pd.DataFrame(data)
        data.columns = cols 
        return data




class BetAndSeatMerger():
    """
    Function created by JL that merges both bet and seat table
    """


    def parse_bets_and_seats(self, bets, seats):
        # merges the bets and seats data to have a userid tagged to each bet
        #Unpacks the seat table so that each account_id is seperated
        all_seats = self.unpack_seats(seats)
        #Creates a column seat with same value as position
        #Essentially renames position->seat
        bets['seat'] = bets['position'].apply(self.parse_position)

        bets = bets.drop('position', axis=1)
        
        all_bets = self.merge_and_fill(bets, all_seats)
        return all_bets

    def parse_position(self, position):
        if type(position) == str:
            if('_' in position):
                return int(position.split('_')[1])
            else:
                return position

        else:
            return position


    def unpack_seats(self, seats):
        # convert our different seat columns to a single column
        all_seats = None
        cols = ['seats_0', 'seats_1', 'seats_2', 'seats_3', 'seats_4', 'seats_5', 'seats_6']
        for c in cols:
            seat_number = int(c.split('_')[1])
            seat = seats[['time', 'table_id']]
            seat['account_id'] = seats[c]
            seat['seat'] = seat_number
            if all_seats is None:
                all_seats = seat
            else:
                all_seats = pd.concat([all_seats, seat])
        return all_seats


    def merge_and_fill(self, bets, all_seats):

        # merge seats onto bets and fill
        all_bets = None
        #Retreives all unique table id from bets
        for table_id in bets['table_id'].unique():
            
            #Retreives all unique bets
            #For a specific table id and specific seat fetch all the corresponding rows
            for seat in bets['seat'].unique():
                
                #Fetch all rows that match the table_id and seat specified above
                bets1 = bets[(bets['table_id'] == table_id)&(bets['seat'] == seat)]
                #If none exist skip
                if len(bets1) == 0:
                    continue
                
                #In the seats db fetch all the rows with table_id and seat from bets table
                seats1 = all_seats[(all_seats['table_id'] == table_id)&(all_seats['seat'] == seat)]

                seats1['account_id'] = seats1['account_id'].fillna('')
                #return bets1, seats1
                bets_merged = pd.concat([bets1, seats1])
                bets_merged = bets_merged.sort_values(by='time', ascending=True)
                bets_merged['account_id'] = bets_merged['account_id'].fillna(method='ffill')
                #return bets_merged
                bets_merged = bets_merged[~bets_merged['_gameid'].isnull()]
                #return bets_merged
                if all_bets is None:
                    all_bets = bets_merged
                else:
                    all_bets = pd.concat([all_bets, bets_merged])
        return all_bets
